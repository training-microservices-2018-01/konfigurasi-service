# Aplikasi konfigurasi-service #

Aplikasi yang bertugas menyediakan konfigurasi untuk semua aplikasi lain. Konfigurasi yang dipublish diambil dari [repository konfigurasi](https://gitlab.com/training-microservices-2018-01/konfigurasi).

## Enable Encryption ##

Edit file `[JAVA_HOME]/jre/lib/security/java.security`. Tambahkan/edit baris berikut:

```
crypto.policy=unlimited
```

## Cara Menjalankan ##

1. Pasang environment variable

   * Windows

        set ENCRYPT_KEY=rahasia

   * Linux

        export ENCRYPT_KEY=rahasia

2. Test dulu apakah sudah terpasang

   * Windows

        echo %ENCRYPT_KEY%

   * Linux

        echo $ENCRYPT_KEY

3. Jalankan aplikasi

        mvn spring-boot:run

## Cara Encrypt Value ##

Menggunakan CURL (command line)

```
curl http://localhost:8888/encrypt -d password-db-production
```

Menggunakan Postman 

![Cara encrypt value dengan Postman](docs/img/postman-encrypt-config.png)
